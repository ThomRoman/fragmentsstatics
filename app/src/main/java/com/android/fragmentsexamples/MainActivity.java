package com.android.fragmentsexamples;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.android.fragmentesexamples.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}